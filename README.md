Print & Play Hanafuda
===

A printable set of real-size hanafuda playing cards.

Navigate to https://Roboe.gitlab.io/PrintHanafuda and follow the print instructions there.

It was made quickly thanks to the `cm` unit available in CSS.


License
===

[Hanafuda SVG images](https://commons.wikimedia.org/wiki/Category:SVG_Hanafuda_(black_border)) in the `images` folder are courtesy of [Louie Mantia](https://commons.wikimedia.org/wiki/User:Louiemantia) on Wikimedia Commons and provided under the [CC BY-SA 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/deed.en).

The code in this repo can be reused under the [MIT License](LICENSE).
