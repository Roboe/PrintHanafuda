#! /usr/bin/env sh
# Docs: https://meta.wikimedia.org/wiki/User-Agent_policy

[ ! "$(command -v apt)" ] && echo "apt is required" && exit 1
WGET_VERSION="$(apt version wget)" # HACK Quickly way to get the wget version

[ ! "$(command -v wget)" ] && echo "wget is required" && exit 1
\wget \
  --user-agent="PrintHanafudaBot/0.0 (https://gitlab.com/Roboe/PrintHanafuda) wget/$WGET_VERSION" \
  --no-clobber \
  --directory-prefix=images \
  --input-file=wikimedia-commons-category-svg-hanafuda-links.txt


FILES_TO_DOWNLOAD="$(wc -l wikimedia-commons-category-svg-hanafuda-links.txt | cut -d ' ' -f 1)"
FILES_ALREADY_DOWNLOADED="$(find images/*.svg | wc -l)"
echo "Downloaded files: $FILES_ALREADY_DOWNLOADED/$FILES_TO_DOWNLOAD"
